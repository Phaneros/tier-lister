var tierListItems = [];

let flagFiles = [
    "images/abbotsford.png",
    "images/anmore.png",
    "images/belcarra.png",
    "images/british_columbia.png",
    "images/burnaby.png",
    "images/chilliwack.png",
    "images/city_of_langley.png",
    "images/city_of_north_vancouver.png",
    "images/coquitlam.png",
    "images/delta.png",
    "images/district_of_hope.png",
    "images/district_of_north_vancouver.png",
    "images/franco-columbiens.png",
    "images/harrison_hot_springs.gif",
    "images/kent.gif",
    "images/maple_ridge.jpg",
    "images/mission.gif",
    "images/musqueam_indian_band.png",
    "images/new_westminster.png",
    "images/pitt_meadows.png",
    "images/port_coquitlam.png",
    "images/port_moody.png",
    "images/richmond.png",
    "images/skwxwu7mesh_uxwumixw_squamish.png",
    "images/surrey.png",
    "images/township_of_langley.png",
    "images/tsawwassen_logo.jpg",
    "images/tsleil_waututh_nation.png",
    "images/vancouver.png",
    "images/west_vancouver.png",
    "images/white_rock.png",
];

let initRow = (rowNode) => {
    rowNode.ondragover = function(ev) {
        ev.preventDefault();
    }
    rowNode.ondrop = function(ev) {
        ev.preventDefault();
        let container = rowNode.querySelectorAll(".container")[0];
        let rankableItem = JSON.parse(ev.dataTransfer.getData('rankableItem'));
        let prevParentId = ev.dataTransfer.getData('prevParentId');

        if (container.id != prevParentId) {
            container.appendChild(createItemNode(rankableItem));
            removeItem(document.getElementById(prevParentId), rankableItem.name);
        }
    }
}

let init = () => {
    let containers = document.querySelectorAll(".container");
    let rowNodes = document.querySelectorAll(".row");
    let unsortedZone = document.getElementById('unsortedZone');
    let chooseFilesButton = document.getElementById("addItemInput");
    let clearItemsButton = document.getElementById("clearItemsButton");
    let loadedImageContainer = document.getElementById("loadedImageContainer");
    let loadFlagsButton = document.getElementById("loadFlagsButton");

    tierListItems.forEach(item => {
        unsortedZone.appendChild(createItemNode(item));
    });

    rowNodes.forEach(initRow);

    chooseFilesButton.onchange = (ev) => {
        for (let fileIndex = 0; fileIndex < chooseFilesButton.files.length; ++fileIndex) {
            let file = chooseFilesButton.files[fileIndex];
            let reader = new FileReader();
            reader.onload = (ev)  => {
                let image = document.createElement("img");
                image.src = ev.target.result;
                image.hidden = true;
                loadedImageContainer.appendChild(image);
                let newItem = {
                    "name": file.name.split(".")[0].replaceAll("_", " "),
                    "img": image.src,
                };
                tierListItems.push(newItem);
                unsortedZone.appendChild(createItemNode(newItem));
            };
            reader.readAsDataURL(file);
        }
        chooseFilesButton.value = null;
    }

    loadFlagsButton.onclick = (ev) => {
        removeAllItems(containers);
        flagFiles.forEach(flagFile => {
            let newItem = {
                "name": flagFile
                    .split(".")[0].replaceAll("_", " ")
                    .split("/")[1],
                "img": flagFile,
            };
            tierListItems.push(newItem);
            unsortedZone.appendChild(createItemNode(newItem));
        });
    }

    clearItemsButton.onclick = (ev) => { removeAllItems(containers) }
}

function createItemNode(item) {
    let node = document.createElement('div');
    let img = document.createElement('img');
    let label = document.createElement('p');

    img.src = item.img;
    label.innerText = item.name;

    node.classList.add('rankableItem');
    node.appendChild(img);
    node.appendChild(label);
    node.label = item.name;
    node.draggable = true;
    node.ondragstart = function(ev) {
        ev.dataTransfer.setData('rankableItem', JSON.stringify(item));
        ev.dataTransfer.setData('prevParentId', node.parentElement.id);
    }

    return node;
}

function removeItem(container, itemName) {
    let itemNodes = container.childNodes;

    itemNodes.forEach(node => {
        if (node.label && node.label.includes(itemName)) {
            node.remove();
        }
    });
}

let removeAllItems = (containers) => {
    containers.forEach(container => {
        for (let nodeIndex = container.childNodes.length - 1; nodeIndex >= 0; --nodeIndex) {
            let node = container.childNodes[nodeIndex];
            if (node.label && new Array(...node.classList).includes("rankableItem")) {
                node.remove();
            }
        }
    });
    let loadedImageContainer = document.getElementById("loadedImageContainer");
    for (let nodeIndex = loadedImageContainer.childNodes.length - 1; nodeIndex >= 0; --nodeIndex) {
        loadedImageContainer.childNodes[nodeIndex].remove();
    }
    tierListItems = [];
}

window.onload = init;